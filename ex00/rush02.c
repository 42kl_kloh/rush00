/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stun-ism <stun-ism@student.42kl.edu>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/20 22:18:37 by stun-ism          #+#    #+#             */
/*   Updated: 2021/02/21 00:03:32 by stun-ism         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print_edges(int x, int y, int c, int r)
{
	if ((c == 1 && r == 1) || (c == x && r == 1))
		ft_putchar('A');
	else if ((c == 1 && r == y) || (c == x && r == y))
		ft_putchar('C');
	else
		ft_putchar('B');
}

void	rush(int x, int y)
{
	int r;
	int c;

	r = 1;
	while (r <= y)
	{
		c = 1;
		while (c <= x)
		{
			if (c == x || c == 1 || r == y || r == 1)
			{
				print_edges(x, y, c, r);
			}
			else
				ft_putchar(' ');
			if (c == x)
				ft_putchar('\n');
			c++;
		}
		r++;
	}
}
