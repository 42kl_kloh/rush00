/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: stun-ism <stun-ism@student.42kl.edu>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/20 22:16:35 by stun-ism          #+#    #+#             */
/*   Updated: 2021/02/21 00:23:35 by stun-ism         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);
void	rush(int x, int y);

int		main(void)
{
	rush(5, 3);
	ft_putchar('\n');
	rush(5, 1);
    ft_putchar('\n');
	rush(1, 1);
    ft_putchar('\n');
	rush(2, 2);
    ft_putchar('\n');
	rush(1, 5);
    ft_putchar('\n');
	rush(4, 4);
    ft_putchar('\n');
	rush(0, 5);
    ft_putchar('\n');
	rush(5, 0);
    ft_putchar('\n');
	rush(-1, -5);
    ft_putchar('\n');
	rush(1, 9);
    ft_putchar('\n');
	rush(5, -5);
    ft_putchar('\n');
	rush(123, 42);
    ft_putchar('\n');
	rush(3, 3);
	return (0);
}
